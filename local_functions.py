import numpy as np
import networkx as nx
from scipy.spatial import distance_matrix
from random import sample as rsample
from random import choices as rchoices
from itertools import product as iproduct
import pickle
import os
import time


################################################################################
### Functions for drawing ######################################################
################################################################################

def make_axis_visible(ax, xlim=None, ylim=None, xticks=None, yticks=None):
    ax.axis("on")
    if xlim is not None:
        ax.set_xlim(xlim)
    if xlim is not None:
        ax.set_ylim(ylim)
    if xlim is not None:
        ax.xaxis.set_ticks(xticks)
    if xlim is not None:
        ax.yaxis.set_ticks(yticks)
        
    ax.tick_params(left=True, bottom=True, labelleft=True, labelbottom=True)
    
    
################################################################################
### Functions for making spatial graphs ########################################
################################################################################

def cuboid_adjacency_matrix(edge_lengths, norm=True):
    '''Create adjacency matrix for a (hyper-)cuboid in d dimensions with 
    geodesic edge length `edge_lengths.'''


    #create empty adjacency matrix
    d = len(edge_lengths)
    num_vertices = np.product(edge_lengths)
    adj_matrix = np.zeros((int(num_vertices),int(num_vertices)))

    #add edges
    # compare https://brilliant.org/discussions/thread/adjacency-matrix-for-a-square-lattice/
    for v in range(num_vertices):
        #print('v',v)
        for i in range(d):
            #print ('i',i)
            set_index1 = int(np.floor(v/np.prod(edge_lengths[:i+1])))
            #print('set_index1',set_index1)
            set_index2 = int(np.floor((v+np.prod(edge_lengths[:i]))
                                      /np.prod(edge_lengths[:i+1])))
            #print('set_index2',set_index2)
            if set_index1 == set_index2:
                #print('add edge', v, int(v+np.prod(edge_lengths[:i])))
                adj_matrix[v, int(v+np.prod(edge_lengths[:i]))] = 1.0    
            
    #symmetrize
    adj_matrix += adj_matrix.T
    
    #normalize to Frobenius norm=1
    if norm:
        # use Frobenius norm
        #adj_matrix = adj_matrix/float(np.sum(adj_matrix))
        # use spectral norm
        adj_matrix = adj_matrix/np.max(np.abs(np.linalg.eigvals(adj_matrix)))
    
    return adj_matrix

def cuboid_number_of_edges(lattice):
    '''Compute the number of edges in a lattice of specified shape. Input must
    be a tuple, list, or other iterable.'''
    
    if len(lattice) > 1:
        lattice_arr = np.array(lattice)
    
        num_edges = (lattice_arr[0] * (cuboid_number_of_edges(lattice_arr[1:]))
                     + (lattice_arr[0] - 1) * np.prod(lattice_arr[1:]))
        return num_edges
    else:
        return lattice[0]-1


def random_geometric_graph(n, m, box=(1.0,1.0), set_load=True):
    '''Create a random geometric graph with `n` nodes and `m` edges in a 
    rectangular patch/ cuboid/ hypercuboid with edge lengths `edge_lengths`.
    I don't know what `set_load` does.
    
    Returns a `nx.Graph()` (undirected graph) which has values for the node 
    attribute `position`.'''
    
    # determine dimension
    d = len(box)
    
    # sample node positions
    positions = np.empty((d, n))
    for i in range(d):
        positions[i] = np.random.uniform(high=box[i], size=n) 
        #TODO: in future add other distributions too
    positions = positions.T
    
    # compute distances for pairs of nodes
    distances = distance_matrix(positions, positions)
    
    # change format from distance matrix to 1d array of tuples (i,j, distance)
    Is = np.arange(n)*np.ones((n,n))
    Js = Is.T
    distances = (np.stack((np.ravel(Is), np.ravel(Js), np.ravel(distances)))).T
    
    # sort node pairs by distances
    sorted_indices = np.argsort(distances[:,-1])
    distances = distances[sorted_indices]
    
    # create undirected graph with n nodes
    g = nx.empty_graph(n, create_using=nx.Graph())
    
    # add node positions to graph
    for i in range(n):
        g.nodes[i]['position']=positions[i]

    # add edges
    g.add_edges_from(zip(distances[n:n+m,0], # first n would be self-edges
                         distances[n:n+m,1])) 
        
    # return graph
    return g
    

def spatial_lattice_graph(nodes=None, edges=None, lattice=None, box=None, 
                          set_load=True):
    '''Create a lattice graph with `n` nodes and `m` edges in a 
    rectangular patch/ cuboid/ hypercuboid with edge lengths `edge_lengths`.
    I don't know what `set_load` does.
    
    Returns a `nx.Graph()` (undirected graph) which has values for the node 
    attribute `position`.'''
    
    # determine dimension
    if lattice is not None:
        d = len(lattice)
    elif box is not None:
        d = len(box)
    else:
        d = 2
        
    # if none given, set box to square/cube/hypercube with edge length 1
    if box is None:
        box = tuple([1.0 for i in range(d)])

    # if none given, set lattice shape to be square and match desired number of nodes
    if lattice is None:
        if nodes is None: nodes = 10**d
        #print('create geodesic edge lengths')
        # get real d-th root of n
        polynomial = np.zeros(d+1)
        polynomial[0], polynomial[-1] = 1.0, -float(nodes)
        root = np.abs(np.roots(polynomial)[0])
        # use ceil of this root for geodesic edge lengths
        num_of_nodes_in_each_direction = int(np.ceil(root))
        # create d-tuple for geodesic edge lengths
        lattice = tuple([num_of_nodes_in_each_direction for i in range(d)])
        #print(lattice)
        
    elif len(box) != d:
        print('''Error in spatial_lattice_graph: Keyword arguments for lattice and  
              box must have same length.''')
        return 0
    
    if nodes is not None:
        if np.product(lattice) < nodes:
            print('''Error in spatial_lattice_graph: lattice too small for graph with
                  desired number of nodes.''')
            return 0
    if edges is not None:
        if cuboid_number_of_edges(lattice) < edges:
            print('''Error in spatial_lattice_graph: lattice *definitely* too small for 
                  graph with desired number of nodes.''')
            return 0
        
    # set desired number of nodes to number of nodes in lattice, if none is given
    if nodes is None:
        nodes = np.product(lattice)
    # set desired number of edges to number of edges in lattice, if none is given 
    if edges is None:
        edges = cuboid_number_of_edges(lattice)
    # create lattice positions
    deltas = [box[i]/(lattice[i]-1) for i in range(d)] 
    coordinates = [deltas[i]*np.arange(lattice[i]) for i in range(d)][::-1]
    positions = list(iproduct(*coordinates))
    
    # create adjacency matrix 
    A = cuboid_adjacency_matrix(lattice, norm=False)
    
    # subsample nodes if needed
    if len(positions) > nodes:
        #print('Status in spatial_lattice_graph: Subsample', len(positions),
        #      'nodes to create graphs with', nodes, 'nodes.')
        sample = np.sort(rsample(range(len(positions)),nodes))        
        positions = [ positions[i] for i in sample ]
        A = A[np.ix_(sample,sample)]
    elif len(positions) < nodes:
        # this shouldnt happen
        print('Error in spatial_lattice_graph: lattice too small for graph with',
              nodes, 'nodes.')
        return 0
    
    # subsample edges if needed
    if np.sum(A) > 2*edges:
        #print('Status in spatial_lattice_graph: Subsample', int(np.sum(A)/2),
        #      'edges to create graphs with', edges, 'edges.')
        num_current_edges = int(np.sum(A)/2)
        num_extra_edges = num_current_edges - edges
        # find edges from adjacency matrix
        Is, Js = np.triu(A).nonzero()
        # sample extra edges from all edges
        extra_edges_ids = rsample(range(num_current_edges), num_extra_edges)
        # remove extra edges
        for i in extra_edges_ids:
            A[Is[i], Js[i]] = 0
            A[Js[i], Is[i]] = 0
            
    elif np.sum(A) < 2*edges:
        print('''Error in spatial_lattice_graph: unlucky draw or lattice or number 
              of nodes too small for graph with desired number of edges.''')
        return 0
        
    # create graph from adjacency matrix
    g = nx.from_numpy_matrix(A, create_using=nx.Graph)
    
    # add node positions as node attributes
    for i in range(nodes):
        g.nodes[i]['position']=positions[i]

    # return graph
    return g
    

def spatial_graph(n, m, edge_lengths=(1.0,1.0), grid=False):
    '''Generate a spatial graph - either a grid or a random geometric graph. 
    Returns a networkX graph objects. Nodes have an attribute 'position'. 
    Edges have attributes 'length' and 'load'.''' 
    
    # The length depends on positions of nodes. The initial load for each edge is 0. 
    # Do I want loads on edges?
    
    if grid:
        return spatial_lattice_graph(n, m, edge_lengths=edge_lengths)
    else:
        return random_geometric_graph(n, m, edge_lengths=edge_lengths)    
    
    
def spatial_graph_with_nests(graph):
    '''Makes a copy of a graph that has an additional node property `is_nest`, 
    which is true for the left-most and right-most node, and is False for all 
    other nodes.'''
    
    # get nodes
    vertices = graph.nodes()
    num_vertices = len(vertices)
    
    # get node positions
    positions = [(i, vertices[i]['position']) for i in vertices]
    sorted_positions = sorted(positions, key=lambda x: x[1][0])
    
    # left-most and right-most node
    left = sorted_positions[0][0]
    right = sorted_positions[-1][0]
    
    # make left-most node and right-most node nests
    nests = [left, right]
        
    # add node Boolean nest property as node attributes
    for i in range(num_vertices):
        if i in nests:
            g.nodes[i]['is_nest']=True
        else:
            g.nodes[i]['is_nest']=False
            
    # return graph
    return g



################################################################################
### Choice algorithms for ants #################################################
################################################################################

def rankEdge_probabilities(loads, q_explore):
    '''Array with probabilities for branches to be chosen by an ant using the
    rankEdge algorithm for a given sequence of pheromone loads.'''
    
    # rank branches by pheromone loads
    ranks = (np.array(loads).argsort()).argsort()
    reverse_ranks = np.max(ranks)-ranks
    
    # calculate probabilities 
    probs = (1-q_explore)*q_explore**reverse_ranks
    
    # for the lowest ranking edge, there is no reject scenario, 
    # so we remove the (1-q_explore) factor
    probs[probs.argmin()] /= (1-q_explore)
    
    #normalize
    probs = probs/np.sum(probs)
    
    return probs


def noisyRankEdge_probabilities(loads, q_explore, noise=0.1):
    '''RankEdge algorithm with noise.'''
    
    noisy_loads = loads+np.random.normal(scale=noise, size=len(loads))
                                         
    return rankEdge_probabilities(noisy_loads, q_explore)    

    
def powerWeighted_probabilities(loads, power):
    '''Array with probabilities for branches to be chosen by an ant using the
    powerWeighted algorithm for a given sequence of pheromone loads.'''
    
    # calculate probabilities
    probs = np.array(loads)**power
    
    #normalize
    probs = probs/np.sum(probs)
    
    return probs


def noisyPowerWeighted_probabilities(loads, power, noise=0.1):
    '''Array with probabilities for branches to be chosen by an ant using the
    noisy powerWeighted algorithm for a given sequence of pheromone loads.'''
    
    noisy_loads = loads+np.random.normal(scale=noise, size=len(loads))
                                         
    return powerWeighted_probabilities(noisy_loads, power) 


################################################################################
### Classes and functions for ABM simulation ###################################
################################################################################        

### helper functions ###########################################################

def edge_filter_function(visibility_dict):
    ''' Define filter function for edges in `initialize_temporal_graph()`.'''
        
    def ff(u,w):
        if (u,w) in visibility_dict.keys():
            return visibility_dict[(u,w)]
        elif (w,u) in visibility_dict.keys():
            return visibility_dict[(w,u)]
        elif u==w:
            # do not initialize self edges at this stage!
            # (use `update_temporal_graph()` for that later)
            return False
        else:
            raise ValueError('This should never have happened.')
            # because edge_visibility_dict should have all non-selfedges
            # as keys!   
                    
    return ff


def color_nodes(self, nest='red', other='grey', food='blue'):
    '''Generate list of colors for drawing ant network nodes.'''
    
    colors = []
    for i in self.nodes():
        if self.nodes[i]['is_nest']:
            colors = colors + [nest]
        elif self.nodes[i]['nutrients']:
            colors = colors + [food]
        else:
            colors = colors + [other]
            
    return colors


def color_edges(self, pheromone='lime', other='black'):
    '''Generate list of colors for drawing ant network edges.'''
    
    colors = []
    for e in self.edges():
        if self[e[0]][e[1]]['pheromone']:
            colors = colors + [pheromone]
        else:
            colors = colors + [other]
            
    return colors


def thicken_edges(self, min_val=None, max_val=None, key='pheromone',
                  min_width=1, max_width=12, default_width=1):
    '''Generate list of thicknesses for drawing ant network edges.'''
    
    # set min and max values for scale
    if min_val is None:
        min_val = min(list(nx.get_edge_attributes(self, key).values()))
    if max_val is None:
        max_val = max(list(nx.get_edge_attributes(self, key).values()))
        
    # get edge thicknesses
    thickness = []
    for e in self.edges():
        if self[e[0]][e[1]][key]:
            # where is pheromone value on scale from min_val to max_val?
            key_on_scale = (self[e[0]][e[1]][key]-min_val)/(max_val-min_val)
            # compute thickness
            w = min_width+(max_width-min_width)*key_on_scale
            # add to list
            thickness = thickness + [w]
        else:
            # if no pheromone, go with default value for thickness
            thickness = thickness + [default_width]

    return thickness
    
    
### classes ###################################################################

class Ant:
    '''Class is an object that two attributes: 1. an origin, and 2. whether it
    lays pheromone.'''
    
    def __init__(self, origin=0):
        self.origin = origin
        self.lays_pheromone = True  
        self.is_new_at_destination = False

        
class AntNetwork(nx.DiGraph):
    '''Class is a tuple of a static (underlying) graph of all possible 
    vegetation and a temporal graph of visible/usable vegetation. The static 
    graph is an undirected graph stored as a networkx digraph. The temporal 
    graph is a networkx subgraph view of the static graph.'''
    
    def __init__(self, graph=None):#, g, filter_edge=lambda x: True):
        
        if graph is None:
            super().__init__()
        else:
            # initialize instance with a directed graph with bidirectional 
            # edges
            super().__init__(nx.DiGraph(nx.Graph(graph)))
        
            # add self-edges at all nodes
            for node in self.nodes():
                self.add_edge(node,node)
            
        # initialize node and edge attributes
        nx.set_node_attributes(self, False, 'is_nest')
        nx.set_node_attributes(self, 0.0, 'nutrients')
        nx.set_edge_attributes(self, [], 'ants')
        nx.set_edge_attributes(self, True, 'is_visible')
        nx.set_edge_attributes(self, 0.0, 'pheromone')
        
        # update edge visibility
        self.update_selfedge_visibility()
        
        
    def hard_copy(self):
        ''' Create a hard copy of an AntNetwork instance.'''
        
        # set static graph
        an2 = AntNetwork(self.get_graph())
        # set temporal graph
        an2.set_visibility(self.get_visibility())
        # set pheromone
        an2.set_pheromone(dict(zip(self.edges(), self.get_pheromone())))
        # set nests
        an2.set_nests(self.get_nests())
        # set nutrients
        an2.set_foodsources(self.get_foodsources())
        # set ants
        an2.set_ants(self.get_ants())
        
        return an2
    
    ### functions for extracting nx graphs #######################################
    
    def get_graph(self):
        '''Return an instance of a networkx graph that has the same structure and
        attributes as the ant network. (This is necessary because some networkx
        function can be unstable on an instance of the class `AntNetwork`.'''
        
        return nx.DiGraph(self)
        
        
    def get_temporal_graph(self):
        '''Return a subgraph view of the ant network that only includes the
        currently visible edges.'''
        
        ff = lambda x, y: self[x][y]['is_visible']
        tg = nx.subgraph_view(self.get_graph(), filter_edge = ff)
        
        return tg
    
    
    ### functions for nest nodes #################################################
    
    def get_nests(self):
        '''Return list of nodes that are ant nests.'''
        
        # get 'is_nest' dictionary
        is_nest = nx.get_node_attributes(self, 'is_nest')
        
        if len(is_nest) == 0:
            # there are no nests; return empty list
            return []
        else:
            # turn dictionary into list
            nests = [node for node in self.nodes() 
                     if is_nest[node]]
            return nests
    
    
    def set_nests(self, nests):
        '''Set list of nodes that are ant nests.'''
        
        for node in self.nodes():
            if node in nests:
                # set label
                self.add_nest(node)
            else:
                # set label
                self.remove_nest(node)

                
    def remove_nest(self, node):
        '''Remove `node` from list of nests in an ant network.'''
        
        self.nodes[node]['is_nest'] = False

        
    def add_nest(self, node):
        '''Add `node` from list of nests in an ant network.'''
        
        self.nodes[node]['is_nest'] = True
        
        
    def set_two_nests(self):
        '''Create two nests at opposite end of static spatial graph of an 
        ant network.'''
        
        # clear previous nests
        self.set_nests([])
    
        # get node positions
        positions_dict = nx.get_node_attributes(self, 'position')
        positions_list = [(i, positions_dict[i]) for i in positions_dict.keys()]
        sorted_positions = sorted(positions_list, key=lambda x: x[1])
    
        # left-most and right-most node
        left = sorted_positions[0][0]
        right = sorted_positions[-1][0]
    
        # make left-most node and right-most node nests
        self.add_nest(left)
        self.add_nest(right)        

        
    ### functions for food source nodes #############################################
    
    def remove_foodsource(self, node):
        '''Remove `node` from list of food sources in an ant network.'''
        
        self.nodes[node]['nutrients'] = 0
        

    def add_foodsource(self, node, amount=100):
        '''Add `node` from list of food sources in an ant network.'''
        
        self.nodes[node]['nutrients'] = amount

        
    def get_foodsources(self):
        '''Get list of food sources in an ant network.'''
        
        # get nutrients dictionary
        nutrients = nx.get_node_attributes(self, 'nutrients')
        
        if len(nutrients) == 0:
            # there are no food sources; return empty list
            return []
        else:
            # get list of food sources (and their nutrient values)
            # from nutrients dictionary 
            sources = [(i, nutrients[i]) for i in self.nodes.keys() 
                       if nutrients[i] > 0]
            return sources
                   
    
    def set_foodsources(self, sources, amount=100):
        '''Set list of food sources in an ant network. In the process, old food
        sources get removed. All new food sources are initialized with the same
        amount of nutrients given by `amount`.'''
        
        for node in self.nodes():
            if node in sources:
                self.add_foodsource(node, amount=amount)
            else:
                self.remove_foodsource(node)

                
    def feed_at_node(self, node):
        '''Simulates an ant feeding at a the food source `node` by decreasing 
        the amount of nutrients at `node` by 1.'''
        
        if self.nodes[node]['nutrients'] == 0:
            # cannot feed at nodes where there is nothing to eat
            raise ValueError('Node has no nutrients.')
                
        else:
            self.nodes[node]['nutrients'] -= 1
        
            if self.nodes[node]['nutrients'] == 0:
                # if food source is depleted, the node becomes a normal node
                self.remove_foodsource(node)
   
    
    ### pheromone functions ####################################################
    
    def get_pheromone(self):
        '''Get list of pheromone levels on edges.'''
        
        # get list of pheromone levels
        levels = [self.edges[e[0], e[1]]['pheromone'] for e in self.edges()]
        # turn into array
        levels = np.array(levels)
        
        return levels
    
    
    def set_pheromone(self, value_dict, empty_canopy=True):
        '''Set pheromone levels in ant network.'''
        
        if empty_canopy:
            # clear previous pheromone levels
            nx.set_edge_attributes(self, 0.0, 'pheromone')
            
        # set new pheromone levels
        for e in value_dict.keys():
            self[e[0]][e[1]]['pheromone'] = value_dict[e]
            self[e[1]][e[0]]['pheromone'] = value_dict[e]
        
        
    def add_pheromone(self, e0, e1, amount=1):
        '''Add pheromone to edge `(e0,e1)`.'''
        
        self[e0][e1]['pheromone'] += 1.0
        if e0 != e1:
            self[e1][e0]['pheromone'] += 1.0
        
        
    def pheromone_decay(self, q_decay):
        '''Simulate decay of pheromone levels and update pheromone levels on all 
        edges of an ant network.'''
        
        for e in self.edges():
            # get old value
            old_value = self.edges[e[0], e[1]]['pheromone']
            # update value
            self.edges[e[0], e[1]]['pheromone'] = (old_value*(1.0-q_decay))
            
            
    def initialize_pheromone(self, degree=1, amount=5, empty_canopy=True):
        '''Initialize pheromone attributes so that each nest is connected to its
        nearest neighboring nests by a pheromone path. The number of neighbouring 
        nests that are connected to a nest via pheromone paths is given by 
        `degree`. Pheromone paths are shortest paths. The amount of pheromone on 
        each edge in a path is specified by `amount`.'''
        
        # get nests
        nests = self.get_nests()
        
        # get distances between nests
        distances = [[ np.linalg.norm(np.array(self.nodes()[i]['position'])
                                      -np.array(self.nodes()[j]['position']))
                      for i in nests] for j in nests]
        distances = np.array(distances)
        
        # for each nest, rank other nests by distance
        ranks = np.empty(distances.shape, dtype=int)
        for i in range(len(ranks)):
            ranks[i] = np.argsort(np.argsort(distances[i]))
        
        # get temporal graph
        tg = self.get_temporal_graph()
        
        # find shortest paths
        edges_for_dict = []
        already_connected = []
        for i, n in enumerate(nests):
            for j in range(len(nests)):
                if i!=j and ranks[i,j] < degree+1 and (j,i) not in already_connected:
                    #print('add path', n, nests[j])
                    # a list of nodes on a shortest path between nests
                    node_list = nx.shortest_path(tg, source=n, target=nests[j])
                    #print(node_list)
                    # turn into list of edges
                    edge_list = list(zip(node_list, np.roll(node_list,1)))[1:]
                    #print(edge_list)
                    # add to overall list of edges that will get pheromone
                    edges_for_dict = edges_for_dict + edge_list
                    # mark nest pair as connected by pheromone
                    already_connected = already_connected + [(i,j)]
                    
        # make dictionary
        edges_for_dict = list(set(edges_for_dict)) # get rid of duplicate edges
        value_dict = {}
        for e in edges_for_dict:
            value_dict[e] = amount
        #print('vd', value_dict)
        
        # set pheromone
        self.set_pheromone(value_dict, empty_canopy=empty_canopy)


    ### ant functions ##########################################################
    
    def get_ant_counts(self):
        '''Return number of ants on each directed edge in ant network.'''
        
        # get list of ant counts
        counts = [len(self.edges[e[0],e[1]]['ants']) for e in self.edges()]
        # turn into array
        counts = np.array(counts)
        
        return counts
    
    
    def add_new_ants(self, i, j, num):
        '''Add `num` new ants to directed edge `(i,j)` in an AntNetwork. New
        ants go to the back of the queue if there are ants at `(i,j)` already.'''
        
        self[i][j]['ants'] = self[i][j]['ants'] + [Ant() for i in range(num)]
        
        
    def get_ants(self):
        '''Return a dictionary of lists of ants on directed edges in an 
        AntNetwork.'''
        
        ants = nx.get_edge_attributes(self, 'ants')
        return ants
    
    
    def set_ants(self, value_dict):
        '''Set ant lists at directed edges to values in `value_dict`. All 
        edges not included in `value_dict` are set to have no ants on them.'''
        
        # clear canopy of ants
        nx.set_edge_attributes(self, [], 'ants')        
        nx.set_edge_attributes(self, value_dict, 'ants')
    
    
    def populate_nests(self, num, empty_canopy=True):
        '''Populate each nest in graph with `num` ants. If `empty_canopy` is 
        True, all the ant count at non-nest nodes is set to zero. This option 
        should be used for first initialization of ants counts on the network 
        (and afterwards when appropriate).'''
        
        if empty_canopy:
            # clear canopy of ants before populating nests
            nx.set_edge_attributes(self, [], 'ants')
        for nest in self.get_nests():
            # populate nest
            self.edges[nest, nest]['ants'] = [Ant(origin=nest)
                                              for i in range(num)]
            

    def move_ant_from(self, e, algorithm=None):
        '''Move an ant from an directed edge `e` to another directed edge that 
        that connects to the end point of `e` . Use `algorithm` to choose an 
        edgeif there is more than one edge starting at the end point of `e`. 
        Return 1 if the ant arrives at nest from which it did not originate.
        Return 0 otherwise.'''
        
        # select algorithm:
        if algorithm is None:
            # if none is given, set algorithm to be noisyRankEdge with 
            # q_explore=0.2
            algorithm = lambda x: noisyRankEdge_probabilities(x, 0.2)
            
        # select ant
        try:
            ant = self.edges[e[0],e[1]]['ants'][0]
        except:
            raise ValueError('Cannot move ants from empty edge.')
        
        # ants who have arrived at their destination in the last timestep
        # stay for one time step
        if ant.is_new_at_destination:
            # move ant from edge to destination (i.e. self-edge)
            # first reset ant attribute
            ant.is_new_at_destination = False
            # list ist of ants at destination
            ant_list = self.edges[edge[1],edge[1]]['ants']
            # add moving ant at the end of list
            ant_list = ant_list + [ant]
            self.edges[edge[1],edge[1]]['ants'] = ant_list
            # remove moving ant from ant list at old edge
            self.edges[e[0],e[1]]['ants'].pop(0)
        
            return 0
        
        # otherwise use algorithm to route ant
        candidates = [outEdge for outEdge in self.edges(e[1]) 
                      if outEdge[1]!=e[0] 
                      and self[outEdge[0]][outEdge[1]]['is_visible']]

        if len(candidates) > 1 and not ant.lays_pheromone:
            # if ant has more than one edge to choose from it is not 
            # in returner mode (anymore)
            ant.lays_pheromone = True
        
        # is ant stuck in dead-end?
        if len(candidates) == 0:
            if self[e[1]][e[0]]['is_visible']: 
                # yes; go back where you came from
                new_edge = (e[1],e[0])
                # ants is now in returner mode and does not lay pheromone
                ant.lays_pheromone = False
            else:
                ## even worse: ant stuck on isolated node?
                # such an unlucky ant! you have to stay where you are...
                new_edge = e
        else:
            # ant is not stuck in dead end!
            # get pheromone levels for candidate edges: for each edge the total 
            # pheromone load is the sum of pheromones laid by ants travelling 
            # in each direction
            pheromone_levels = [self.edges[c[0],c[1]]['pheromone']
                                #+self.edges[c[1],c[0]]['pheromone'])
                                for c in candidates]
            
            # compute probabilities
            probs = algorithm(pheromone_levels)
        
            # choose edge
            new_edge = rchoices(candidates, weights=probs, k=1)[0]
        
        # move ant from old edge to new edge
        # start by getting list of ants at new edge
        ant_list = self.edges[new_edge[0],new_edge[1]]['ants']
        # add moving ant at the end of list
        ant_list = ant_list + [ant]
        self.edges[new_edge[0],new_edge[1]]['ants'] = ant_list
        # remove moving ant from ant list at old edge
        self.edges[e[0],e[1]]['ants'].pop(0)
        
        # lay pheromone on new edge
        if ant.lays_pheromone and not new_edge[0] == new_edge[1]:
            # store pheromone concentration for a bidirectional edge on 
            # the first of its two directed edges
            self.add_pheromone(new_edge[0], new_edge[1]) 
        
        # successful arrival?
        # if the destination is a nest and ...
        if (self.nodes[new_edge[1]]['is_nest'] 
            # ... it is not the nest that the ant originated from ...
            and ant.origin != new_edge[1]):
            # ... reset ant origin ...
            ant.origin = new_edge[1]
            # ... and report one successful arrival
            return 1
        
        else:
            # or no successful arrival
            return 0
        

    def move_ants(self, algorithm=None):
        '''From each edge, move the first ant in the queue. Return the number
        of arrivals of ants at a nest.'''
        
        # count ants on each edge
        ant_counts = self.get_ant_counts()
        # it is important to have ant counts as a separate list so it does not 
        # update automatically while we move ants (or else some ants might 
        # travel several edges in one time step)
        
        # move ants
        arrivals = 0
        for i, e in enumerate(self.edges()):
            if ant_counts[i]: #has ants
                arrivals += self.move_ant_from(e, algorithm=algorithm)
                    
        return arrivals

    
    ### functions for temporal graph ###########################################
    
    def make_visible(self, i, j):
        '''Set visibility of edge (i,j) to `True`.'''
        
        self.edges[i, j]['is_visible'] = True
        self.edges[j, i]['is_visible'] = True
    
    
    def make_invisible(self, i, j):
        '''Set visibility of edge (i,j) to `False`.'''

        self.edges[i, j]['is_visible'] = False
        self.edges[j, i]['is_visible'] = False
        
        
    def get_visibility(self):
        '''Return a dictionary of Boolean values indicating whether an edge 
        from the spatial graph of an ant network is visible in its temporal 
        graph.'''
        
        visibility = nx.get_edge_attributes(self, 'is_visible')
        return visibility
    
    
    def set_visibility(self, edge_dict):
        '''Set visibility of edges with values in `edge_dict`. All edges not 
        included in `edge_dict` are set invisible unless they are self-edges.
        Self-edges not include `edge_dict` retain their previous visibility 
        value.'''
        
        for e in self.edges():
            if e in edge_dict.keys():
                #print('set for', e, 'to', edge_dict[e])
                self[e[0]][e[1]]['is_visible'] = edge_dict[e]
                self[e[1]][e[0]]['is_visible'] = edge_dict[e]
            elif (e[1],e[0]) in edge_dict.keys():
                self[e[0]][e[1]]['is_visible'] = edge_dict[(e[1],e[0])]
                self[e[1]][e[0]]['is_visible'] = edge_dict[(e[1],e[0])]
            elif e[0]!=e[1]:
                #print('set for', e, 'to standard False')
                self[e[0]][e[1]]['is_visible'] = False
                self[e[1]][e[0]]['is_visible'] = False  
    
    
    def update_selfedge_visibility(self):
        '''Update visibility of self-edges. Self-edge visibility requires
        updates when food sources deplete or nests are added or removed.'''
        
        for node in self.nodes():
            if (self.nodes[node]['nutrients'] > 0
                or self.nodes[node]['is_nest']):
                self.edges[node, node]['is_visible'] = True
            else:
                self.edges[node, node]['is_visible'] = False  
                
                
    def initialize_temporal_graph(self, visibility=0.9, connected=True, 
                                  max_runs=100):
        '''Set edge visibility so that a fraction `visibility` of edges is 
        visible and a fraction `1-visibility` of edges is invisible. Visible and
        invisible edges are chosen uniformly at random from non-self edges. If
        an edge is visible in one direction it is also visible in the other 
        direction. If `connected` is `True`, select visibility of edges such
        that the visible graph is connected. If a connected solution cannot be 
        found in `max_runs`, set visibility of all non-self edges to `True`.'''
        
        # make filter list
        undirected_edges = [e for e in self.edges() if e[0] < e[1]]
        num_edges = len(undirected_edges)
        num_visible_edges = int(np.round(visibility*num_edges))
        num_invisible_edges = num_edges-num_visible_edges
        edge_vis_list = np.array(([False]*num_invisible_edges 
                                  + [True]*num_visible_edges))

        for i in range(max_runs):
            
            # shuffle visibility list
            edge_vis_list = np.random.permutation(edge_vis_list)           
            
            if not connected:
                # not further checks needed; set edge visibility
                for j, e in enumerate(undirected_edges):
                        self[e[0]][e[1]]['is_visible']=edge_vis_list[j]
                        self[e[1]][e[0]]['is_visible']=edge_vis_list[j]
                return 0
                
            else:                
                # turn edge visibility list into dictionary
                edge_vis_dict = dict(zip(undirected_edges, edge_vis_list))
                # make edge filter from visibility dictionary
                f1 = edge_filter_function(edge_vis_dict)
                # create subgraph view with edge-visibility filter
                gview = nx.subgraph_view(self, filter_edge=f1) 
            
                # check if subgraph view is weakly connected
                if nx.is_weakly_connected(gview):
                    
                    # if yes, set the edge visibility in the ant network
                    for j, e in enumerate(undirected_edges):
                        self[e[0]][e[1]]['is_visible']=edge_vis_list[j]
                        self[e[1]][e[0]]['is_visible']=edge_vis_list[j]
                        
                    # return how many tries it took
                    return i #TODO
                
                # if not connected, try again...
                
        # if we do not find a connected graph in max_runs
        print('''Warning in initialize_temporal_graph: Could not initialize
              connected temporal graph with specified visibility. Use static
              graph as temporal graph instead.''') 
        
        for e in self.edges():
            if e[0] != e[1]:
                self[e[0]][e[1]]['is_visible'] = True   
    
 
    def perturb_temporal_graph(self, m): 
        '''Simulate random perturbations to the visible graph. Attempt to remove
        `m` edges uniformly at random from the visible graph and add `m` other 
        edges uniformly at random from the static graph to the temporal graph. 
        If `m` is not an integer, `n` edges are removed (and added) where `n` is 
        an integer-valued random variable that takes values floor(`m`) and 
        ceil(`m`) and has expected value `m`.'''
        
        # choose integer m
        if isinstance(m, int):
            int_m = m
        else:
            weights = [1-(m-np.floor(m)), m-np.floor(m)]
            int_m = int(np.floor(m)+rchoices([0,1], weights=weights, k=1)[0])
            
        # get lists of currently visible (and thus removable) undirected 
        # non-self edges and list of invisible (and thus addable) undirected 
        # non-self edges
        edge_list = list(self.edges())
        removable_edge_list = []
        invisible_edge_list = []
        for e in edge_list:
            if e[0] < e[1]:
                if self[e[0]][e[1]]['is_visible']:
                    removable_edge_list = removable_edge_list + [e]
                else:
                    invisible_edge_list = invisible_edge_list + [e]
            
        # check for both lists if they are long enough
        if len(invisible_edge_list) < int_m:
            raise IndexError('Not enough invisible edges to make visible.')
        if len(removable_edge_list) < int_m:
            raise IndexError('Not enough visible edges to make invisible.')

        # select m edges for removal
        vanishing_edges = rchoices(removable_edge_list, k=int_m)
        
        # select m edges to be added
        new_edges = rchoices(invisible_edge_list, k=int_m)
        
        # update visibility of added and removed edges
        for e in new_edges:
            self[e[0]][e[1]]['is_visible'] = True
            self[e[1]][e[0]]['is_visible'] = True
        for e in vanishing_edges:
            self[e[0]][e[1]]['is_visible'] = False
            self[e[1]][e[0]]['is_visible'] = False 
            # also reset pheromone value for vanishing edges
            self[e[0]][e[1]]['pheromone'] = 0.0
            self[e[1]][e[0]]['pheromone'] = 0.0
            

################################################################################
### functions for simulation ###################################################
################################################################################

def graph_filename(lattice=None, random=None, box=None):
    '''Make filename for lattice graph or random geometric graph.'''
    
    # check and update incomplete input arguments
    if lattice is None and random is None:
        raise IOError("Expect a value for either `lattice` or `random`. None given.")
    if lattice is not None and random is not None:
        raise IOError("Expect a value for either `lattice` or `random`, but not both!")
    if box is None:
        box = (1,1)
    
    # get parameters from keyword arguments
    if lattice is not None:
        s = 'lattice'
        parameters = lattice
    else:
        s = 'rgm'
        parameters = random
    
    # create string for filename from parameters
    for n in parameters:
        s += str(n) + '-'
    s = s[:-1] + '_box'
    for n in box:
        s+= str(n) + '-'
    s = s[:-1]
    
    return s


def run_filename(alg=None, q=None, noise=None, q_decay=None, 
                 ants=None, volatility=None, timesteps=None,
                 sampling_rate=None, iteration=None):
    '''Make consistent name strings for npy files for an run of the agent-based
    model. All keyword arguments must be set.'''
    
    if alg is None:
        raise(IOError, "run_filename requires input for algorithm 'alg'.")
    if q is None:
        raise(IOError, "run_filename requires input for parameter 'q'.")
    if noise is None:
        raise(IOError, "run_filename requires input for parameter 'noise'.")
    if q_decay is None:
        raise(IOError, "run_filename requires input for parameter 'q_decay'.")
    if ants is None:
        raise(IOError, "run_filename requires input for 'ants'.")
    if volatility is None:
        raise(IOError, "run_filename requires input for 'volatility'.")
    if timesteps is None:
        raise(IOError, "run_filename requires input for 'timesteps'.")
    if sampling_rate is None:
        raise(IOError, "run_filename requires input for 'sampling_rate'.")
    if iteration is None:
        raise(IOError, "run_filename requires input for 'iteration'.")
        
    name = ('_'+alg+str(q)+'_noise'+str(noise)+'_decay'+str(q)
            +'_ants'+str(ants)
            +'_volatility'+str(volatility)
            +'_time'+str(timesteps)
            +'_samplingrate'+str(sampling_rate)
            +'_iteration'+str(iteration)
            +'.npy')
    
    return name


def run_abm(g, graph_name, visibility=0.9, algorithm='noisyRankEdge', 
            q=None, q_explore=0.2, q_power=2.0, noise=0.2, q_decay=0.02,
            ants=100, volatility=0, timesteps=1000, sampling_rate=100,
            iterations=50):
    '''Run agent-based model for foraging tree ants.'''
    
    # identify algorithm and set algorithm parameters
    if algorithm in ['nre', 'noisyRankEdge', 'NoisyRankEdge']:
        algorithm_string = 'nre'
        if q is None:
            q = q_explore
        alg = lambda x: noisyRankEdge_probabilities(x, q, noise=noise)
    elif algorithm in ['re','rankEdge','RankEdge']:
        algorithm_string = 're'
        if q is None:
            q = q_explore
        alg = lambda x: rankEdge_probabilities(x, q)
    elif algorithm in ['npw', 'noisyPowerWeighted', 'NoisyPowerWeighted']:
        algorithm_string = 'npw'
        if q is None:
            q=q_power
        alg = lambda x: noisyPowerWeighted_probabilities(x, q, noise=noise)
    elif algorithm in ['pw', 'powerWeighted', 'PowerWeighted']:
        algorithm_string = 'pw'
        if q is None:
            q=q_power
        alg = lambda x: powerWeighted_probabilities(x, q)
    else:
        raise NameError("Unknown algorithm in keyword arguments of run_abm.")
    
    # initialize ant network   
    AN = AntNetwork(graph=g)
    
    # include two nests
    AN.set_two_nests()
    # allocate half of the total number of ants to each nest
    AN.populate_nests(int(ants/2))
    # initialize edge visibility
    AN.initialize_temporal_graph(visibility=visibility)
    # update selfedge visibility
    AN.update_selfedge_visibility()
    
    # figure out dimensions of arrays for saving data
    num_true_edges = AN.number_of_edges()
    num_samples = int(timesteps/sampling_rate)
    
    # run abm    
    for iteration in range(iterations):
        
        print('i',iteration)
        
        #initialize arrays for saving simulation data
        visibility = np.zeros((num_samples, num_true_edges), dtype='bool')
        sightings = np.zeros((num_samples, num_true_edges), dtype='int')
        pheromone_level = np.zeros((num_samples, num_true_edges))
        arrivals = np.zeros(num_samples, dtype='int')
        new_arrivals = 0
        
        # run simulation
        for t in range(timesteps):
            # collect info on network (info from BEGINNGING of each time step)
            if timesteps % sampling_rate == 0:
                visibility[int(t/sampling_rate)] = AN.get_visibility()
                sightings[int(t/sampling_rate)] = AN.get_ant_counts()
                pheromone_level[int(t/sampling_rate)]=AN.get_pheromone()
                arrivals[int(t/sampling_rate)] = new_arrivals
                new_arrivals = 0
            
            # move ants and count ant arrivals at nests
            new_arrivals += AN.move_ants(algorithm=alg) 
            
            # make changes to the network
            # 1. update pheromone levels
            AN.pheromone_decay(q_decay)
            # 2. perturb/ update network structure
            AN.perturb_temporal_graph(volatility)
        
        # save graph
        if not os.path.exists(graph_name): os.mkdir(graph_name)
        path = graph_name + '/'
        outfile = open(path+graph_name+'.pickle','wb')
        pickle.dump(g, outfile)
        outfile.close()
        
        # get filenames for simulation data      
        fn = run_filename(alg=algorithm_string, q=q, q_decay=q_decay, 
                          noise=noise, ants=ants, volatility=volatility, 
                          timesteps=timesteps, sampling_rate=sampling_rate,
                          iteration=iteration)
        
        # save data
        np.save(path+'arrivals'+fn, arrivals)
        np.save(path+'pheromone'+fn, pheromone_level)
        np.save(path+'sightings'+fn, sightings)
        np.save(path+'visibility'+fn, visibility)
        
################################################################################